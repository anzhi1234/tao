package com.tao.orderserver.vo;

import lombok.Data;

/**
 * @Author zja
 * @Date 2021/6/25
 * @Version 1.0
 */
@Data
public class User {

    private String userName;


    private String password;
}
