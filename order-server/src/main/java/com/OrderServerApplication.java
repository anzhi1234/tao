package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.netty.NettyWebServer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import rx.internal.util.RxRingBuffer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

@SpringBootApplication
@EnableDiscoveryClient
public class OrderServerApplication {

    public static void main(String[] args) {
        int a = 0;
        new ArrayList<>();
        new LinkedList<>();

        System.out.println("----------订单服务开始启动-----------");
        SpringApplication.run(OrderServerApplication.class, args);
        System.out.println("----------订单服务启动完成-----------");
    }

}
