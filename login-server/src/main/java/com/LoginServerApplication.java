package com;

import com.google.common.escape.ArrayBasedUnicodeEscaper;
import com.login.loginserver.controller.LoginController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.netty.NettyWebServer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.ConcurrentHashMap;

@SpringBootApplication
@EnableDiscoveryClient
public class LoginServerApplication  {


    public static void main(String[] args) throws ClassNotFoundException {


        System.out.println("-----------登录服务开始启动--------------");
        SpringApplication.run(LoginServerApplication.class, args);

        System.out.println("-----------登录服务启动完成--------------");



        System.out.println("aaa");

    }



}
